# Used to generate an RGB image
import numpy as np
import matplotlib.pyplot as plt
from astropy.visualization import make_lupton_rgb

# Used to generate the current path
import sys


def generate_RGB_image(red, green, blue, path, shape_x=299, shape_y=299, stretch=0.27):

    image_r = np.full((shape_x, shape_y), red)
    image_g = np.full((shape_x, shape_y), green)
    image_b = np.full((shape_x, shape_y), blue)

    # Saves the generated image
    make_lupton_rgb(image_r, image_g, image_b, stretch=stretch, filename=path)


def gen_rotation_matrix(yaw=0.0, pitch=0.0, roll=0.0):

    R_yaw = np.eye(3)
    R_yaw[0, 0] = np.cos(yaw)
    R_yaw[0, 2] = np.sin(yaw)
    R_yaw[2, 0] = -np.sin(yaw)
    R_yaw[2, 2] = np.cos(yaw)

    R_pitch = np.eye(3)
    R_pitch[1, 1] = np.cos(pitch)
    R_pitch[1, 2] = -np.sin(pitch)
    R_pitch[2, 1] = np.sin(pitch)
    R_pitch[2, 2] = np.cos(pitch)

    R_roll = np.eye(3)
    R_roll[0, 0] = np.cos(roll)
    R_roll[0, 1] = -np.sin(roll)
    R_roll[1, 0] = np.sin(roll)
    R_roll[1, 1] = np.cos(roll)

    return np.dot(R_yaw, np.dot(R_pitch, R_roll))


def print_info(dictionary, output_path=None):

    output = ""
    for key in dictionary:
        for el in key[:-1]:
            output += '{:.5f}'.format(el) + ','
        output += '{:.5f}'.format(key[-1]) + ','

        for el in dictionary[key][:-1]:
            output += el[1] + ' ' + '{:.5f}'.format(el[2]) + ','
        output += dictionary[key][-1][1] + ' ' + '{:.5f}'.format(dictionary[key][-1][2]) + '\n'

    if not output_path:
        print(output)

    else:
        with open(output_path, mode="a") as out:
            out.write(output)


def get_current_path():

    return "/".join(sys.argv[0].split("/")[:-2])

def get_best_z(file_csv):

    with open(file_csv, mode='r') as file:

        n = 0
        sum_5 = 0
        sum_15 = 0
        sum_25 = 0

        for line in file:
            
            n += 1

            items = line.split(',')

            z = float(items[2])

            if z == -5.0:
                if len(items) > 11:
                    vals = items[11].strip().split(" ")
                    sum_5 += float(vals[1])
                else:
                    for val in items[6:]:
                        vals = val.strip().split(" ")
                        if vals[0] == 'jeep':
                            sum_5 += float(vals[1])
                            break

            elif z == -15.0:
                if len(items) > 11:
                    vals = items[11].strip().split(" ")
                    sum_15 += float(vals[1])
                else:
                    for val in items[6:]:
                        vals = val.strip().split(" ")
                        if vals[0] == 'jeep':
                            sum_15 += float(vals[1])
                            break

            elif z == -25.0:
                if len(items) > 11:
                    vals = items[11].strip().split(" ")
                    sum_25 += float(vals[1])
                else:
                    for val in items[6:]:
                        vals = val.strip().split(" ")
                        if vals[0] == 'jeep':
                            sum_25 += float(vals[1])
                            break

    n /= 3

    return sum_5/n, sum_15/n, sum_25/n

def get_class_results(file_csv):

    results = dict()

    with open(file_csv, mode='r') as file:
        lines = file.readlines()

        for line in lines:

            c, a = line.split(',')[6].split(' ')
            c = c.strip()
            a = float(a)

            if c in results:
                results[c][0] += 1
                results[c][2] += a
                if a > 0.7: 
                    results[c][1] += 1
            else:
                results[c] = [1, 1, a] if a > 0.7 else [1, 0, a]
        
    for c in results:
        results[c][2] /= results[c][0]
    
    return results