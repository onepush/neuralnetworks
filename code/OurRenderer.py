# Used to handle paths
import os

# Used to generate images from 3d models with 6d poses
from renderer import Renderer
from utils import gen_rotation_matrix

class OurRenderer:

    def __init__(self, obj_name, mtl_name, background_name, camera_distance, angle_of_view, dir_path):

        self.obj_name = obj_name
        self.dir_path = dir_path
        self.angle_of_view = angle_of_view
        self.camera_distance = camera_distance

        self.renderer = Renderer(
            obj_f=dir_path + "/3DModels/" + obj_name, mtl_f=dir_path + "/3DModels/" + mtl_name,
            background_f=dir_path + "/Backgrounds/" + background_name if background_name else None,
            camera_distance=camera_distance, angle_of_view=angle_of_view
        )

    def get_picture(self, x=0.5, y=0.5, z=-2, yaw=0.0, pitch=0.0, roll=0.0, save_image=False, show_image=False):

        self.renderer.prog["x"].value = x
        self.renderer.prog["y"].value = y
        self.renderer.prog["z"].value = z

        # Defines the orientation of the 3D object
        R_obj = gen_rotation_matrix(yaw, pitch, roll)
        self.renderer.prog["R_obj"].write(R_obj.T.astype("f4").tobytes())

        # Takes the picture
        image = self.renderer.render()

        if save_image:
            # Saves the image with a meaningful name in the images folder
            name = "_".join([self.obj_name[:-4], '{:.2f}'.format(x), '{:.2f}'.format(y), '{:.2f}'.format(z),
                             '{:.2f}'.format(yaw), '{:.2f}'.format(pitch), '{:.2f}'.format(roll)]) + ".png"
            image.save(os.path.join(self.dir_path, 'documentation', 'nn_report', 'images', name))

        if show_image:
            # Just shows the image
            image.show()

        return image

