import os
import argparse
# Used to perform random search and gradient descent
from math import tan, radians, pi, log, cos, sin, atan2
from random import uniform
from InceptionModel import InceptionModel
from OurRenderer import OurRenderer

# Used to generate a background image, to generate the current path and to save info on a file
from utils import get_current_path, generate_RGB_image, print_info, get_best_z, get_class_results


def random_search(model, n_iterations, renderer, z_list=[-5, -15, -25]):

    tg = tan(radians(renderer.angle_of_view/2))
    s = tg * abs(renderer.camera_distance - min(z_list))
    # x_interval = [-s, s]
    # y_interval = [-s, s]
    # yaw_interval = [0, 2*pi]
    # pitch_interval = [0, 2*pi]
    # roll_interval = [0, 2*pi]

    results = dict()

    while n_iterations > 0:
        print("Iterations {:d}".format(n_iterations))

        # Randomly pick one combination
        x_value = uniform(-s, s)
        y_value = uniform(-s, s)
        yaw_value = uniform(0, 2*pi)
        pitch_value = uniform(0, 2*pi)
        roll_value = uniform(0, 2*pi)

        for z in z_list:

            factor = (tg * abs(renderer.camera_distance - z)) / s

            image = renderer.get_picture(
                x_value, y_value, z, yaw_value, pitch_value, roll_value)

            # Even if results already contains this key it will replace the value with itself -> not worth checking first
            results[(x_value*factor, y_value*factor, z, yaw_value,
                     pitch_value, roll_value)] = model.inception_predict(image)

        n_iterations -= 1

    return results


def single_param_evaluation(model, renderer, precision=150, z_list=[-5, -15, -25]):

    tg = tan(radians(renderer.angle_of_view/2))
    s = tg * abs(renderer.camera_distance - min(z_list))
    # x_interval = [-s, s]
    # y_interval = [-s, s]
    # yaw_interval = [0, 2*pi]
    # pitch_interval = [0, 2*pi]
    # roll_interval = [0, 2*pi]

    results_x = dict()
    results_y = dict()
    results_yaw = dict()
    results_pitch = dict()
    results_roll = dict()

    rotation_step = 2*pi/precision
    translation_step = 2*s/precision

    x_value = 0
    y_value = 0
    yaw_value = 0
    pitch_value = 0
    roll_value = 0

    for i in range(precision+1):

        for z in z_list:

            factor = (tg * abs(renderer.camera_distance - z)) / s

            translation_pos = -s*factor + i*translation_step*factor
            rotation_pos = rotation_step*i

            # x
            image = renderer.get_picture(
                translation_pos, y_value, z, yaw_value, pitch_value, roll_value)
            results_x[(translation_pos, z)] = model.inception_predict(image)

            # y
            image = renderer.get_picture(
                x_value, translation_pos, z, yaw_value, pitch_value, roll_value)
            results_y[(translation_pos, z)] = model.inception_predict(image)

            # yaw
            image = renderer.get_picture(
                x_value, y_value, z, rotation_pos, pitch_value, roll_value)
            results_yaw[(rotation_pos, z)] = model.inception_predict(image)

            # roll
            image = renderer.get_picture(
                x_value, y_value, z, yaw_value, rotation_pos, roll_value)
            results_pitch[(rotation_pos, z)] = model.inception_predict(image)

            # pitch
            image = renderer.get_picture(
                x_value, y_value, z, yaw_value, pitch_value, rotation_pos)
            results_roll[(rotation_pos, z)] = model.inception_predict(image)

    return results_x, results_y, results_yaw, results_pitch, results_roll


def gradient_descent(model, renderer, n_iterations, z, h=0.001, gamma=0.01, initial_point=None):

    s = tan(radians(renderer.angle_of_view/2)) * \
        abs(renderer.camera_distance - z)

    # Get stating weights
    x_value = uniform(-s, s) if initial_point == None else initial_point[0]
    y_value = uniform(-s, s) if initial_point == None else initial_point[1]
    yaw_value = uniform(0, 2*pi) if initial_point == None else initial_point[2]
    pitch_value = uniform(
        0, 2*pi) if initial_point == None else initial_point[3]
    roll_value = uniform(
        0, 2*pi) if initial_point == None else initial_point[4]

    parameters = [x_value, y_value, sin(yaw_value), cos(yaw_value), sin(
        pitch_value), cos(pitch_value), sin(roll_value), cos(roll_value)]

    results = dict()

    while n_iterations > 0:
        print("Iterations {:d}:\t{s}".format(n_iterations, s=parameters))

        for i in range(len(parameters)):

            # These two lists are used to apply temporary changes to compute gradient
            l1 = parameters[:]  # This is a way to copy a list
            l2 = parameters[:]

            l1[i] += h/2
            l2[i] -= h/2

            # Compute the image with parameters in l1
            image1 = renderer.get_picture(x=l1[0], y=l1[1], z=z, yaw=atan2(l1[2], l1[3]), pitch=atan2(
                l1[4], l1[5]), roll=atan2(l1[6], l1[7]), save_image=False, show_image=False)

            # Compute the image with parameters in l2
            image2 = renderer.get_picture(x=l2[0], y=l2[1], z=z, yaw=atan2(l2[2], l2[3]), pitch=atan2(
                l2[4], l2[5]), roll=atan2(l2[6], l2[7]), save_image=False, show_image=False)

            # Compute the gradient given the two images
            gradient = (-log(1-model.jeep_probability(image1)) +
                        log(1-model.jeep_probability(image2))) / h

            # Update the parameter with Vanilla update rule
            parameters[i] -= gamma * gradient

        image = renderer.get_picture(parameters[0], parameters[1], z, yaw=atan2(parameters[2], parameters[3]), pitch=atan2(
            parameters[4], parameters[5]), roll=atan2(parameters[6], parameters[7]), save_image=False, show_image=False)
        results[(parameters[0], parameters[1], z, atan2(parameters[2], parameters[3]), atan2(
            parameters[4], parameters[5]), atan2(parameters[6], parameters[7]))] = model.inception_predict(image)
        n_iterations -= 1

    return [parameters[0], parameters[1], z, atan2(parameters[2], parameters[3]), atan2(parameters[4], parameters[5]), atan2(parameters[6], parameters[7])], results


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--random_search', type=int, default=0)
    parser.add_argument('--gradient', type=int, default=0)
    parser.add_argument('--single_param_evaluation', type=int, default=0)
    parser.add_argument('--classes_evaluation', type=int, default=0)
    parser.add_argument('--z_evaluation', type=int, default=0)
    args = parser.parse_args()

    # Uncomment to generate a background image. Notice: that it will be 299x299 -> The standard value for the inception model
    # generate_RGB_image(0.485, 0.456, 0.406, "./Backgrounds/background.png", shape_x=299, shape_y=299)

    dir_path = get_current_path()

    inception_model = InceptionModel()
    renderer = OurRenderer(obj_name='Jeep.obj', mtl_name='Jeep.mtl', background_name='background.png',
                           camera_distance=2.0, angle_of_view=16.426, dir_path=dir_path)

    z_deltas = [-5, -15, -25]

    if args.random_search:
        n = 100
        random_search_results = random_search(
            model=inception_model, n_iterations=n, renderer=renderer, z_list=z_deltas)
        print_info(random_search_results, output_path=os.path.join(
            dir_path, 'random_search_results.txt'))

    if args.gradient:
        n = 300
        parameters, history = gradient_descent(
            model=inception_model, renderer=renderer, n_iterations=n, z=-5, initial_point=[0, 0, 0.0, 0.0, 0.0])
        print(parameters)
        print_info(history, output_path=os.path.join(
            dir_path, 'gradient_descent_results.csv'))

        image = renderer.get_picture(x=parameters[0], y=parameters[1], z=-5, yaw=parameters[2],
                                     pitch=parameters[3], roll=parameters[4], save_image=False, show_image=True)

        print(inception_model.inception_predict(image))

    if args.single_param_evaluation:

        results_x, results_y, results_yaw, results_pitch, results_roll = single_param_evaluation(
            inception_model, renderer, precision=250, z_list=[-5, -15, -25])

        print_info(results_x, output_path=os.path.join(
            dir_path, 'documentation', 'results', 'x_variation_evaluation.csv'))
        print_info(results_y, output_path=os.path.join(
            dir_path, 'documentation', 'results', 'y_variation_evaluation.csv'))
        print_info(results_yaw, output_path=os.path.join(
            dir_path, 'documentation', 'results', 'yaw_variation_evaluation.csv'))
        print_info(results_pitch, output_path=os.path.join(
            dir_path, 'documentation', 'results', 'pitch_variation_evaluation.csv'))
        print_info(results_roll, output_path=os.path.join(
            dir_path, 'documentation', 'results', 'roll_variation_evaluation.csv'))

    if args.classes_evaluation:

        classes_results = get_class_results(os.path.join(
            dir_path, 'documentation', 'results', 'random_search_results.csv'))
        f = open(os.path.join(dir_path, 'documentation', 'results',
                              'classes_evaluation_results.csv'), mode="w")

        for el in classes_results:
            items = classes_results[el]
            f.write(el + ',' + str(items[0]) + ',' + str(items[1]
                                                         ) + ',' + '{:.4f}'.format(items[2]) + '\n')

    if args.z_evaluation:

        mean_5, mean_15, mean_25 = get_best_z(os.path.join(
            dir_path, 'documentation', 'results', 'random_search_results.csv'))

        print('The average probability of jeep with z=-5 is: {}\n'
            'The average probability of jeep with z=-15 is: {}\n'
            'The average probability of jeep with z=-25 is: {}'.
                format(mean_5, mean_15, mean_25))
