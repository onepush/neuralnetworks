# Used to generate graphs
import matplotlib.pyplot as plt

# Used to evaluate the s variables
from math import tan, radians

# Used to handle paths
import os

def parse_single_param_results(input_file, normalize=False):

    x_5 = list()
    y_5 = list()
    x_15 = list()
    y_15 = list()
    x_25 = list()
    y_25 = list()

    tg = tan(radians(16.426/2))
    s_5 = tg * abs(2 + 5)
    s_15 = tg * abs(2 + 15)
    s_25 = tg * abs(2 + 25)

    with open(input_file, mode='r') as inp:
        
        for line in inp:

            vals = line.split(',')

            for val in vals[2:]:

                val = val.strip()

                if val.startswith('jeep'):
                    prob = val.split(' ')[1]

            out = float(vals[0])

            if float(vals[1]) == -5.0:
                
                if normalize:
                    out /= s_5
                x_5.append(out)
                y_5.append(float(prob))
                
            elif float(vals[1]) == -15.0:
                
                if normalize:
                    out /= s_15
                x_15.append(out)
                y_15.append(float(prob))

            elif float(vals[1]) == -25.0:

                if normalize:
                    out /= s_25

                x_25.append(out)
                y_25.append(float(prob))

            else:
                print('Error! Unexpected z value: {}'.format(vals[1]))
                return

    return [x_5, y_5, x_15, y_15, x_25, y_25]



if __name__ == "__main__":

    # pitch plot
    pitch_res = parse_single_param_results('documentation/results/pitch_variation_evaluation.csv')

    plt.plot(pitch_res[0], pitch_res[1], '-', label='pitch with z=-5')
    plt.plot(pitch_res[2], pitch_res[3], '-', label='pitch with z=-15')
    plt.plot(pitch_res[4], pitch_res[5], '-', label='pitch with z=-25')
    plt.ylabel('Jeep probability')
    plt.legend(loc='upper right')
    plt.title('Pitch Influence')

    plt.savefig(os.path.join('documentation/nn_report/images', 'pitch_influence.png'))
    plt.close()

    # roll plot
    roll_res = parse_single_param_results('documentation/results/roll_variation_evaluation.csv')

    plt.plot(roll_res[0], roll_res[1], '-', label='roll with z=-5')
    plt.plot(roll_res[2], roll_res[3], '-', label='roll with z=-15')
    plt.plot(roll_res[4], roll_res[5], '-', label='roll with z=-25')
    plt.ylabel('Jeep probability')
    plt.legend(loc='upper right')
    plt.title('Roll Influence')

    plt.savefig(os.path.join('documentation/nn_report/images', 'roll_influence.png'))
    plt.close()

    # yaw plot
    yaw_res = parse_single_param_results('documentation/results/yaw_variation_evaluation.csv')

    plt.plot(yaw_res[0], yaw_res[1], '-', label='yaw with z=-5')
    plt.plot(yaw_res[2], yaw_res[3], '-', label='yaw with z=-15')
    plt.plot(yaw_res[4], yaw_res[5], '-', label='yaw with z=-25')
    plt.ylabel('Jeep probability')
    plt.legend(loc='upper right')
    plt.title('Yaw Influence')

    plt.savefig(os.path.join('documentation/nn_report/images', 'yaw_influence.png'))
    plt.close()

    # x plot
    x_res = parse_single_param_results('documentation/results/x_variation_evaluation.csv', normalize=True)

    plt.plot(x_res[0], x_res[1], '-', label='x with z=-5')
    plt.plot(x_res[2], x_res[3], '-', label='x with z=-15')
    plt.plot(x_res[4], x_res[5], '-', label='x with z=-25')
    plt.ylabel('Jeep probability')
    plt.legend(loc='upper right')
    plt.title('X Influence')

    plt.savefig(os.path.join('documentation/nn_report/images', 'x_influence.png'))
    plt.close()

    # y plot
    y_res = parse_single_param_results('documentation/results/y_variation_evaluation.csv', normalize=True)

    plt.plot(y_res[0], y_res[1], '-', label='y with z=-5')
    plt.plot(y_res[2], y_res[3], '-', label='y with z=-15')
    plt.plot(y_res[4], y_res[5], '-', label='y with z=-25')
    plt.ylabel('Jeep probability')
    plt.legend(loc='upper right')
    plt.title('Y Influence')

    plt.savefig(os.path.join('documentation/nn_report/images', 'y_influence.png'))
    plt.close()
