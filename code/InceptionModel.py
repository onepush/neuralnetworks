# Used to predict classes
import numpy as np
from tensorflow._api.v1 import keras as K
from tensorflow._api.v1.keras.applications.inception_v3 import decode_predictions, preprocess_input as inception_preprocess

class InceptionModel:

    def __init__(self):

        self.inception_model = K.applications.inception_v3.InceptionV3()

    def inception_predict(self, image):

        img = np.array(image)
        img = np.expand_dims(img, axis=0)
        img = inception_preprocess(img)

        res = self.inception_model.predict(img)

        res_list = decode_predictions(res)[0]

        if all(el[1] != "jeep" for el in res_list):
            res_list.append((0, "jeep", res[0][609]))

        return res_list

    def jeep_probability(self, image):

        img = np.array(image)
        img = np.expand_dims(img, axis=0)
        img = inception_preprocess(img)

        res = self.inception_model.predict(img)

        return res[0][609]
